package perez.frank.bl.entities.usuario;

public class Usuario {
    protected String cedula;
    protected String nombre;
    protected String apellido1;
    protected String apellido2;
    protected String tiopUsuario;
    protected String clave;

    public Usuario() {

    }

    public Usuario(String cedula, String nombre, String apellido1, String apellido2, String tipoUsuario, String clave) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.tiopUsuario = tipoUsuario;
        this.clave = clave;
    }
    //La clave es un atributo de usuario.
    public Usuario(String cedula, String clave) {
        this.cedula = cedula;
        this.clave = clave;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }


    public String getTiopUsuario() {
        return tiopUsuario;
    }

    public void setTiopUsuario(String tiopUsuario) {
        this.tiopUsuario = tiopUsuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }




}

