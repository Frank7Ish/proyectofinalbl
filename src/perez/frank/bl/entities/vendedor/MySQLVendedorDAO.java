package perez.frank.bl.entities.vendedor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLVendedorDAO {

    public MySQLVendedorDAO(){

    }
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public ArrayList<Vendedor> listarVendedores()throws SQLException {
        ArrayList<Vendedor> listaVendedores = new ArrayList<Vendedor>();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT u.cedula, u.nombre, u.apellido1, u.apellido2, v.direccion, v.correo_electronico, v.clave  FROM vendedor AS v INNER JOIN usuario AS u ON v.cedula = u.cedula ";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);// el rs almacena la información de la base de datos.
            // TENGO QUE RECORRERLA
            while (rs.next()) { //rs.next devuelve true si hay más líneas en el result set. por defecto, al iniciar el ciclo, el rs está en la línea 0.
                Vendedor vendedor = new Vendedor(rs.getString("cedula"), rs.getString("nombre"), rs.getString("apellido1"),
                        rs.getString("apellido2"), rs.getString("direccion"), rs.getString("correo_electronico"),rs.getString("clave"));

                listaVendedores.add(vendedor);

            }

            //conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){

            System.out.println(e.getMessage());
        }
        return listaVendedores;

    }

    public List<Vendedor> findAllPureJdbc() throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            connection = DriverManager.getConnection(strConexion);
            preparedStatement = connection.prepareStatement("SELECT cedula, nombre, apellido1, apellido2, direccion, correo_electronico, clave  FROM vendedor AS v INNER JOIN usuario AS u ON v.cedula = u.cedula ");
            ResultSet rs = preparedStatement.executeQuery();
            return mapResults(rs);
        } catch (SQLException e) {
            System.out.println(e.getMessage());

        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        return null;
    }

    private List<Vendedor> mapResults(ResultSet rs) throws SQLException {
        List<Vendedor> results = new ArrayList<>();
        while (rs.next()) {
            Vendedor vendedor = new Vendedor(rs.getString("cedula"), rs.getString("nombre"), rs.getString("apellido1"), rs.getString("apellido2"), rs.getString("direccion"), rs.getString("correo_electronico"),rs.getString("clave"));

            results.add(vendedor);
        }
        return results;
    }

    public static void registrarVendedor(Vendedor tmpVendedor)throws SQLException, IOException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            String query = "INSERT INTO vendedor(direccion, correo_electronico, cedula, clave) VALUES(?,?,?,?)";
            Connection conn = null;

            PreparedStatement stmt = null;
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.prepareStatement(query);

            stmt.setString(1, tmpVendedor.getDireccion());
            stmt.setString(2, tmpVendedor.getCorreoElectonico());
            stmt.setString(3, tmpVendedor.getCedula());
            stmt.setString(4, tmpVendedor.getClave());
            stmt.execute();

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }


    /*public Vendedor buscarVendedor(String correo, String password)throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT * FROM vendedor WHERE correo_electronico ="+" correo";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);// el rs almacena la información de la base de datos.
            // TENGO QUE RECORRERLA
            while (rs.next()) { //rs.next devuelve true si hay más líneas en el result set. por defecto, al iniciar el ciclo, el rs está en la línea 0.
                Vendedor vendedor = new Vendedor(rs.getString("cedula"), rs.getString("nombre"), rs.getString("primer apellido"), rs.getString("segundo apellido"), rs.getString("direccion"), rs.getString("correo electronico"), rs.getString("clave"));
                return vendedor;

            }

            //conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){

            System.out.println(e.getMessage());
        }

    }*/
}
