package perez.frank.bl.entities.vendedor;

import perez.frank.bl.entities.usuario.Usuario;

import java.util.Objects;

public class Vendedor extends Usuario {
    private String direccion;
    private String correoElectronico;
    private String clave;
    //calificacion se puede hacer hasta que la orden de compra el coleccionista la marque como realizada
    public Vendedor(){

    }

    public Vendedor(String cedula, String nombre, String apellido1, String apellido2, String clave, String direccion, String correElectonico) {
        super(cedula, nombre, apellido1, apellido2, "Vendedor", clave);
        this.direccion = direccion;
        this.correoElectronico = correElectonico;
        this.clave = clave;
    }



    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreoElectonico() {
        return correoElectronico;
    }

    public void setCorreoElectonico(String correElectonico) {
        this.correoElectronico = correElectonico;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vendedor vendedor = (Vendedor) o;
        return Objects.equals(direccion, vendedor.direccion) && Objects.equals(correoElectronico, vendedor.correoElectronico) && Objects.equals(clave, vendedor.clave);
    }

    @Override
    public int hashCode() {
        return Objects.hash(direccion, correoElectronico, clave);
    }

    @Override
    public String toString() {
        return "Vendedor{" +
                "cedula='" + cedula + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", tiopUsuario='" + tiopUsuario + '\'' +
                ", clave='" + clave + '\'' +
                ", direccion='" + direccion + '\'' +
                ", correoElectronico='" + correoElectronico + '\'' +
                ", clave='" + clave + '\'' +
                '}';
    }
}
