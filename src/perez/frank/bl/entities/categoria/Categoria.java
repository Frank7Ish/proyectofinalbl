package perez.frank.bl.entities.categoria;

import perez.frank.bl.entities.estado.Estado;

import java.util.Objects;

public class Categoria {
    private int codigo;
    private String nombre;
    private Estado estado;

    public Categoria(){

    }

    public Categoria(String nombre, Estado estado) {
        this.nombre = nombre;
        this.estado = estado;
    }


    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Categoria categoria = (Categoria) o;
        return codigo == categoria.codigo && Objects.equals(nombre, categoria.nombre) && estado == categoria.estado;
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo, nombre, estado);
    }

    @Override
    public String toString() {
        return "Categoria{" +
                "codigo=" + codigo +
                ", nombre='" + nombre + '\'' +
                ", estado=" + estado +
                '}';
    }
}
