package perez.frank.bl.entities.coleccionista;

import perez.frank.bl.entities.estado.Estado;
import perez.frank.bl.entities.usuario.Usuario;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

public class Coleccionista extends Usuario {
    private String correoElectronico;
    private String avatar;
    private LocalDate fechaDeNacimiento;
    private int edad;
    private Estado estado;
    private double puntuacion;
    private String intereses;


    public Coleccionista(String cedula, String nombre, String apellido1, String apellido2,  String clave, String correoElectronico, String avatar, LocalDate fechaDeNacimiento, int edad,  double puntuacion, String intereses) {
        super(cedula, nombre, apellido1, apellido2, "Coleccionista", clave);
        this.correoElectronico = correoElectronico;
        this.avatar = avatar;
        this.fechaDeNacimiento = fechaDeNacimiento;
        this.edad = edad;
        this.estado = Estado.ACTIVO;
        this.puntuacion = puntuacion;
        this.intereses = intereses;
        this.clave = clave;
    }

    public Coleccionista(String correoElectronico, String clave) {
        this.correoElectronico = correoElectronico;
        this.clave = clave;
    }

    public Coleccionista() {

    }


    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public LocalDate getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setFechaDeNacimiento(LocalDate fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public double getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(double puntuacion) {
        this.puntuacion = puntuacion;
    }

    public String getIntereses() {
        return intereses;
    }

    public void setIntereses(String intereses) {
        intereses = intereses;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coleccionista that = (Coleccionista) o;
        return edad == that.edad && Double.compare(that.puntuacion, puntuacion) == 0 && Objects.equals(avatar, that.avatar) && Objects.equals(fechaDeNacimiento, that.fechaDeNacimiento) && estado == that.estado && Objects.equals(correoElectronico, that.correoElectronico) && Objects.equals(intereses, that.intereses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(avatar, fechaDeNacimiento, edad, estado, correoElectronico, puntuacion, intereses);
    }

    @Override
    public String toString() {
        return "Coleccionista{" +
                "avatar='" + avatar + '\'' +
                ", fechaDeNacimiento=" + fechaDeNacimiento +
                ", edad=" + edad +
                ", estado=" + estado +
                ", correoElectronico='" + correoElectronico + '\'' +
                ", puntuacion=" + puntuacion +
                ", Intereses='" + intereses + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                '}';
    }
}