package perez.frank.bl.entities.coleccionista;

import perez.frank.bl.entities.estado.Estado;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.time.LocalDate;

public class MySQLColeccionistaDAO {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public ArrayList<Coleccionista> listarColeccionistas()throws SQLException {
        ArrayList<Coleccionista> listaColeccionistas = new ArrayList<Coleccionista>();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT * FROM coleccionista";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/universidad?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);// el rs almacena la información de la base de datos.
            // TENGO QUE RECORRERLA
            while (rs.next()) { //rs.next devuelve true si hay más líneas en el result set. por defecto, al iniciar el ciclo, el rs está en la línea 0.
                Coleccionista coleccionista = new Coleccionista(
                        rs.getString("cedula"),
                        rs.getString("nombre"),
                        rs.getString("apellido1"),
                        rs.getString("apellido2"),
                        rs.getString("clave"),
                        rs.getString("correo_electronico"),
                        rs.getString("avatar"),
                        LocalDate.parse(rs.getString("fehca_de_nacimiento")),
                        rs.getInt("edad"), rs.getDouble("puntuacion"),
                        rs.getString("intereses"));

                listaColeccionistas.add(coleccionista);
            }

            //conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){

            System.out.println(e.getMessage());
        }
        return listaColeccionistas;

    }

    public static void registrarColeccionista(Coleccionista tmpColeccionista)throws SQLException, IOException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            String query = "INSERT INTO coleccionista(correo_electronico, avatar, fecha_de_nacimiento, edad, estado, puntuacion, intereses, cedula) VALUES(?,?,?,?,?,?,?,?)";
            Connection conn = null;

            PreparedStatement stmt = null;
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.prepareStatement(query);

            stmt.setString(1, tmpColeccionista.getCorreoElectronico());
            stmt.setString(2, tmpColeccionista.getAvatar());
            stmt.setString(3, tmpColeccionista.getFechaDeNacimiento().toString());
            stmt.setInt(4, tmpColeccionista.getEdad());
            stmt.setString(5, tmpColeccionista.getEstado().getNombre());
            stmt.setDouble(6, tmpColeccionista.getPuntuacion());
            stmt.setString(7, tmpColeccionista.getIntereses());
            stmt.setString(8, tmpColeccionista.getCedula());
            stmt.execute();

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
