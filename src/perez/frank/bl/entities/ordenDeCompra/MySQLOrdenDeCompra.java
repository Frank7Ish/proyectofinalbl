package perez.frank.bl.entities.ordenDeCompra;

import perez.frank.bl.entities.coleccionista.Coleccionista;
import perez.frank.bl.entities.vendedor.Vendedor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.*;
import java.util.ArrayList;

public class MySQLOrdenDeCompra {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public ArrayList<OrdenDeCompra> listarOrdenDeCompras()throws SQLException {
        ArrayList<OrdenDeCompra> listaOrdenDeCompras = new ArrayList<OrdenDeCompra>();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT * FROM orden_de_compra";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/universidad?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);// el rs almacena la información de la base de datos.
            // TENGO QUE RECORRERLA
            while (rs.next()) { //rs.next devuelve true si hay más líneas en el result set. por defecto, al iniciar el ciclo, el rs está en la línea 0.
                OrdenDeCompra ordenDeCompra = new OrdenDeCompra( (Coleccionista) rs.getObject("coleccionista"), (Vendedor) rs.getObject("vendedor"));
                ordenDeCompra.setNumeroDeOrden(rs.getInt("numero_de_orden"));
                listaOrdenDeCompras.add(ordenDeCompra);
            }

            conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){

            System.out.println(e.getMessage());
        }
        return listaOrdenDeCompras;

    }

    public static void registrarOrdenDeCompra(OrdenDeCompra tmpOrdenDeCompra)throws SQLException, IOException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            String query = "INSERT INTO orden_de_compra(id_coleccionista, id_vendedor, id_item) VALUES(?,?,?)";
            Connection conn = null;

            PreparedStatement stmt = null;
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.prepareStatement(query);

            stmt.setString(1, tmpOrdenDeCompra.getColeccionista().getCorreoElectronico());
            stmt.setString(2, tmpOrdenDeCompra.getVendedor().getCorreoElectonico());
            stmt.execute();

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
