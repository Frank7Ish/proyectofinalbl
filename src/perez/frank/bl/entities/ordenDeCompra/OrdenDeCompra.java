package perez.frank.bl.entities.ordenDeCompra;

import perez.frank.bl.entities.coleccionista.Coleccionista;
import perez.frank.bl.entities.item.Item;
import perez.frank.bl.entities.vendedor.Vendedor;

import java.util.ArrayList;
import java.util.Objects;

public class OrdenDeCompra {
    private int numeroDeOrden;
    private Coleccionista coleccionista;
    private Vendedor vendedor;
    private ArrayList<Item> items;

    public OrdenDeCompra(Coleccionista coleccionista, Vendedor vendedor){
        this.coleccionista = coleccionista;
        this.vendedor = vendedor;
        this.items = new ArrayList<Item>();
    }

    public int getNumeroDeOrden() {
        return numeroDeOrden;
    }

    public void setNumeroDeOrden(int numeroDeOrden) {
        this.numeroDeOrden = numeroDeOrden;
    }

    public Coleccionista getColeccionista() {
        return coleccionista;
    }

    public void setColeccionista(Coleccionista coleccionista) {
        this.coleccionista = coleccionista;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrdenDeCompra that = (OrdenDeCompra) o;
        return numeroDeOrden == that.numeroDeOrden && Objects.equals(coleccionista, that.coleccionista) && Objects.equals(vendedor, that.vendedor) && Objects.equals(items, that.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numeroDeOrden, coleccionista, vendedor, items);
    }

    @Override
    public String toString() {
        return "OrdenDeCompra{" +
                "numeroDeOrden=" + numeroDeOrden +
                ", coleccionista=" + coleccionista +
                ", vendedor=" + vendedor +
                ", items=" + items +
                '}';
    }
}
