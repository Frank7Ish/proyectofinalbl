package perez.frank.bl.entities.ofertaDeCompra;

import perez.frank.bl.entities.coleccionista.Coleccionista;
import perez.frank.bl.entities.estado.Estado;
import perez.frank.bl.entities.subasta.Subasta;

import java.util.Objects;

public class OfertaDeCompra {//OFERTAS
    private int codigo;
    private Coleccionista ofertante;
    private double ofertaDeCompra;
    private Estado estado;
    private Subasta subasta;

    public OfertaDeCompra(){

    }

    public OfertaDeCompra(Coleccionista ofertante, double ofertaDeCompra, Subasta subasta) {
        this.ofertante = ofertante;
        this.ofertaDeCompra = ofertaDeCompra;
        this.estado = Estado.OFERTANDO;
        this.subasta = subasta;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Coleccionista getOfertante() {
        return ofertante;
    }

    public void setOfertante(Coleccionista ofertante) {
        this.ofertante = ofertante;
    }

    public double getOfertaDeCompra() {
        return ofertaDeCompra;
    }

    public void setOfertaDeCompra(double ofertaDeCompra) {
        this.ofertaDeCompra = ofertaDeCompra;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Subasta getSubasta() {
        return subasta;
    }

    public void setSubasta(Subasta subasta) {
        this.subasta = subasta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OfertaDeCompra that = (OfertaDeCompra) o;
        return Double.compare(that.ofertaDeCompra, ofertaDeCompra) == 0 && Objects.equals(ofertante, that.ofertante) && estado == that.estado;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ofertante, ofertaDeCompra, estado);
    }

    @Override
    public String toString() {
        return "OrdenDeCompra{" +
                "ofertante=" + ofertante +
                ", ofertaDeCompra=" + ofertaDeCompra +
                ", estado=" + estado +
                '}';
    }
}
