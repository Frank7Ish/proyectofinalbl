package perez.frank.bl.entities.ofertaDeCompra;


import perez.frank.bl.entities.coleccionista.Coleccionista;
import perez.frank.bl.entities.estado.Estado;
import perez.frank.bl.entities.subasta.Subasta;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.*;
import java.util.ArrayList;

public class MySQLOfertaDeCompraDAO {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public ArrayList<OfertaDeCompra> listarOfertaDeCompras()throws SQLException {
        ArrayList<OfertaDeCompra> listaOfertaDeCompras = new ArrayList<OfertaDeCompra>();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT * FROM oferta_de_compra";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/universidad?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);// el rs almacena la información de la base de datos.
            // TENGO QUE RECORRERLA
            while (rs.next()) { //rs.next devuelve true si hay más líneas en el result set. por defecto, al iniciar el ciclo, el rs está en la línea 0.
                OfertaDeCompra ofertaDeCompra = new OfertaDeCompra((Coleccionista) rs.getObject("id_ofertante"), rs.getDouble("oferta_de_compra"), (Subasta) rs.getObject("subasta"));
                ofertaDeCompra.setCodigo(rs.getInt("codigo"));
                ofertaDeCompra.setEstado((Estado) rs.getObject("estado") );
                listaOfertaDeCompras.add(ofertaDeCompra);
            }

            //conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){

            System.out.println(e.getMessage());
        }
        return listaOfertaDeCompras;

    }

    public static void registrarOfertaDeCompra(OfertaDeCompra tmpOfertaDeCompra)throws SQLException, IOException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            String query = "INSERT INTO oferta_de_compra(nombre, apellido1, apellido2, subasta) VALUES(?,?,?,?)";
            Connection conn = null;

            PreparedStatement stmt = null;
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.prepareStatement(query);

            stmt.setObject(1, tmpOfertaDeCompra.getOfertante());
            stmt.setDouble(2, tmpOfertaDeCompra.getOfertaDeCompra());
            stmt.setString(3, tmpOfertaDeCompra.getEstado().getNombre());
            stmt.setInt(4, tmpOfertaDeCompra.getSubasta().getCodigo());
            stmt.execute();

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
