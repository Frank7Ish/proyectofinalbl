package perez.frank.bl.entities.administrador;

import perez.frank.bl.entities.estado.Estado;
import perez.frank.bl.entities.usuario.Usuario;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

public class Administrador extends Usuario {
    private String correoElectronico;
    private String avatar;
    private LocalDate fechaDeNacimiento;
    private int edad;
    private Estado estado;




    public Administrador(String cedula, String nombre, String apellido1, String apellido2, String clave, String correoElectronico,  String avatar, LocalDate fechaDeNacimiento, int edad, Estado estado) {
        super(cedula, nombre, apellido1, apellido2, "Administrador", clave);
        this.correoElectronico = correoElectronico;
        this.avatar = avatar;
        this.fechaDeNacimiento = fechaDeNacimiento;
        this.edad = edad;
        this.estado = estado;

    }


    public Administrador(String correoElectronico, String clave) {
        this.correoElectronico = correoElectronico;
        this.clave = clave;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }


    public LocalDate getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setFechaDeNacimiento(LocalDate fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Administrador that = (Administrador) o;
        return edad == that.edad && Objects.equals(avatar, that.avatar) && Objects.equals(fechaDeNacimiento, that.fechaDeNacimiento) && estado == that.estado && Objects.equals(correoElectronico, that.correoElectronico);
    }

    @Override
    public int hashCode() {
        return Objects.hash(avatar, fechaDeNacimiento, edad, estado, correoElectronico);
    }

    @Override
    public String toString() {
        return "Administrador{" +
                "avatar='" + avatar + '\'' +
                ", fechaDeNacimiento=" + fechaDeNacimiento +
                ", edad=" + edad +
                ", estado=" + estado +
                ", correoElectronico='" + correoElectronico + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                '}';
    }
}
