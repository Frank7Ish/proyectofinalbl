package perez.frank.bl.entities.administrador;

import perez.frank.bl.entities.estado.Estado;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class MySQLAdministradorDAO {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    /*public ArrayList<Administrador> listarAdministradores()throws SQLException {
        ArrayList<Administrador> listaAdministradores = new ArrayList<Administrador>();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT * FROM administrador";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);// el rs almacena la información de la base de datos.
            // TENGO QUE RECORRERLA
            while (rs.next()) { //rs.next devuelve true si hay más líneas en el result set. por defecto, al iniciar el ciclo, el rs está en la línea 0.
                Administrador administrador = new Administrador(rs.getString("correo_electronico"), rs.getString("avatar"), LocalDate.parse(rs.getString("fehca_de_nacimiento")), rs.getInt("edad"),(Estado) rs.getObject("estado"));

                listaAdministradores.add(administrador);
            }

            conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){

            System.out.println(e.getMessage());
        }
        return listaAdministradores;

    }*/

    public static void registrarAdministrador(Administrador tmpAdmin)throws SQLException, IOException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            String query = "INSERT INTO administrador(correo_electronico, cedula, avatar, fecha_de_nacimiento, edad, estado, clave) VALUES(?,?,?,?,?,?,?)";
            Connection conn = null;

            PreparedStatement stmt = null;
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.prepareStatement(query);

            stmt.setString(1, tmpAdmin.getCorreoElectronico());
            stmt.setString(2, tmpAdmin.getCedula());
            stmt.setString(3, tmpAdmin.getAvatar());
            stmt.setString(4, tmpAdmin.getFechaDeNacimiento().toString());
            stmt.setInt(5, tmpAdmin.getEdad());
            stmt.setString(6, tmpAdmin.getEstado().getNombre());
            stmt.setString(7, tmpAdmin.getClave());

            stmt.execute();

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
