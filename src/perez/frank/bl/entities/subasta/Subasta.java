package perez.frank.bl.entities.subasta;

import perez.frank.bl.entities.coleccionista.Coleccionista;
import perez.frank.bl.entities.estado.Estado;
import perez.frank.bl.entities.item.Item;
import perez.frank.bl.entities.ofertaDeCompra.OfertaDeCompra;
import perez.frank.bl.entities.usuario.Usuario;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class Subasta {
    private int codigo;
    private Usuario creador;//Objeto usuario
    private LocalDate fechaDeCreacion;
    private LocalDate fechaDeInicio;
    private LocalDate fechaDeVencimiento;
    private Estado estado;
    private double precioDeInicio;//CUANDO UNA OFERTA DE COMPRA SE GENERA, SE DEBE ACUTALIZAR EL PRECIO DE INICIO
    private ArrayList<Item> items;
    private ArrayList<OfertaDeCompra> listaOfertas;// LA OFERTA DE COMPRA CON EL PRECIO MAS ALTO GANA
    private Coleccionista moderador;//Objeto moderador
    private int autoIncrement = 1;
    //crea orden de compra, item, vendedor, coleccionista


    public Subasta() {
    }

    public Subasta(Usuario creador, LocalDate fechaDeCreacion, LocalDate fechaDeInicio, LocalDate fechaDeVencimiento, double precioDeInicio, Coleccionista moderador) {
        this.codigo = autoIncrement;
        this.creador = creador;
        this.fechaDeCreacion = fechaDeCreacion;
        this.fechaDeInicio = fechaDeInicio;
        this.fechaDeVencimiento = fechaDeVencimiento;
        this.estado = Estado.ACTIVO;
        this.precioDeInicio = precioDeInicio;
        this.moderador = moderador;
        this.items = new ArrayList<Item>();
        this.listaOfertas= new ArrayList<OfertaDeCompra>();
        autoIncrement++;
    }


    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Usuario getCreador() {
        return creador;
    }

    public void setCreador(Usuario creador) {
        this.creador = creador;
    }

    public LocalDate getFechaDeCreacion() {
        return fechaDeCreacion;
    }

    public void setFechaDeCreacion(LocalDate fechaDeCreacion) {
        this.fechaDeCreacion = fechaDeCreacion;
    }

    public LocalDate getFechaDeInicio() {
        return fechaDeInicio;
    }

    public void setFechaDeInicio(LocalDate fechaDeInicio) {
        this.fechaDeInicio = fechaDeInicio;
    }

    public LocalDate getFechaDeVencimiento() {
        return fechaDeVencimiento;
    }

    public void setFechaDeVencimiento(LocalDate fechaDeVencimiento) {
        this.fechaDeVencimiento = fechaDeVencimiento;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public double getPrecioDeInicio() {
        return precioDeInicio;
    }

    public void setPrecioDeInicio(double precioDeInicio) {
        this.precioDeInicio = precioDeInicio;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public ArrayList<OfertaDeCompra> getListaOfertas() {
        return listaOfertas;
    }

    public void setListaOfertas(ArrayList<OfertaDeCompra> listaOfertas) {
        this.listaOfertas = listaOfertas;
    }

    public Coleccionista getModerador() {
        return moderador;
    }

    public void setModerador(Coleccionista moderador) {
        this.moderador = moderador;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subasta subasta = (Subasta) o;
        return Double.compare(subasta.precioDeInicio, precioDeInicio) == 0 && Objects.equals(creador, subasta.creador) && Objects.equals(fechaDeCreacion, subasta.fechaDeCreacion) && Objects.equals(fechaDeInicio, subasta.fechaDeInicio) && Objects.equals(fechaDeVencimiento, subasta.fechaDeVencimiento) && estado == subasta.estado && Objects.equals(items, subasta.items) && Objects.equals(listaOfertas, subasta.listaOfertas) && Objects.equals(moderador, subasta.moderador);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creador, fechaDeCreacion, fechaDeInicio, fechaDeVencimiento, estado, precioDeInicio, items, listaOfertas, moderador);
    }

    //METODO PARA CREAR LA OFERTA

    //METODO QUE GENERE LA ORDE, RECIBE UN OBJETO OFERTA Y HACE EL PROCESO


    @Override
    public String toString() {
        return "Subasta{" +
                "codigo=" + codigo +
                ", creador=" + creador +
                ", fechaDeCreacion=" + fechaDeCreacion +
                ", fechaDeInicio=" + fechaDeInicio +
                ", fechaDeVencimiento=" + fechaDeVencimiento +
                ", estado=" + estado +
                ", precioDeInicio=" + precioDeInicio +
                ", items=" + items +
                ", listaOfertas=" + listaOfertas +
                ", moderador=" + moderador +
                ", autoIncrement=" + autoIncrement +
                '}';
    }
}

