package perez.frank.bl.entities.subasta;

import perez.frank.bl.entities.coleccionista.Coleccionista;
import perez.frank.bl.entities.estado.Estado;
import perez.frank.bl.entities.usuario.Usuario;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class MySQLSubastaDAO {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public ArrayList<Subasta> listarSubastas()throws SQLException {

        ArrayList<Subasta> listaSubastas = new ArrayList<Subasta>();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT * FROM subasta";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);// el rs almacena la información de la base de datos.
            // TENGO QUE RECORRERLA
            while (rs.next()) { //rs.next devuelve true si hay más líneas en el result set. por defecto, al iniciar el ciclo, el rs está en la línea 0.
                Subasta subasta;
                Usuario us = new Usuario();
                us.setCedula(rs.getString("id_creador"));
                Coleccionista col= new Coleccionista();
                col.setCorreoElectronico(rs.getString("id_moderador"));

                subasta = new Subasta(
                        us,
                        LocalDate.parse(rs.getString("fecha_de_creacion")),
                        LocalDate.parse(rs.getString("fecha_de_inicio")),
                        LocalDate.parse(rs.getString("fecha_de_vencimiento")),
                        rs.getDouble("precio_de_inicio"),
                        col);
                subasta.setCodigo(rs.getInt("codigo"));
                listaSubastas.add(subasta);
            }

            conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){

            System.out.println(e.getMessage());
        }
        return listaSubastas;

    }

    public ArrayList<Subasta> listarSubastasDeUsuario(String cedula)throws SQLException {
        ArrayList<Subasta> listaSubastas = new ArrayList<Subasta>();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT * FROM subasta where id_creador=" + cedula;
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);// el rs almacena la información de la base de datos.
            // TENGO QUE RECORRERLA
            while (rs.next()) { //rs.next devuelve true si hay más líneas en el result set. por defecto, al iniciar el ciclo, el rs está en la línea 0.
                Subasta subasta;
                Usuario us = new Usuario();
                us.setCedula(rs.getString("id_creador"));
                Coleccionista col= new Coleccionista();
                col.setCorreoElectronico(rs.getString("id_moderador"));

                subasta = new Subasta(
                        us,
                        LocalDate.parse(rs.getString("fecha_de_creacion")),
                        LocalDate.parse(rs.getString("fecha_de_inicio")),
                        LocalDate.parse(rs.getString("fecha_de_vencimiento")),
                        rs.getDouble("precio_de_inicio"),
                        col);
                subasta.setCodigo(rs.getInt("codigo"));
                listaSubastas.add(subasta);
            }

            conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){

            System.out.println(e.getMessage());
        }
        return listaSubastas;

    }

    public static void registrarSubasta(Subasta tmpSubasta)throws SQLException, IOException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            String query = "INSERT INTO subasta(codigo, id_creador, fecha_de_creacion, fecha_de_inicio, fecha_de_vencimiento, estado, precio_de_Inicio) VALUES(?,?,?,?,?,?,?)";
            Connection conn = null;

            PreparedStatement stmt = null;
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.prepareStatement(query);

            stmt.setInt(1, (int)(Math.random()*(7500-2500+100)+2500));
            stmt.setString(2, tmpSubasta.getCreador().getCedula());
            stmt.setString(3, tmpSubasta.getFechaDeCreacion().toString());
            stmt.setString(4, tmpSubasta.getFechaDeInicio().toString());
            stmt.setString(5, tmpSubasta.getFechaDeVencimiento().toString());
            stmt.setString(6,tmpSubasta.getEstado().getNombre());
            stmt.setDouble(7, (double) tmpSubasta.getPrecioDeInicio());
            //stmt.setString(8,tmpSubasta.getModerador().getCorreoElectronico());
            stmt.execute();

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
