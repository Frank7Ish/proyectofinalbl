package perez.frank.bl.entities.item;

import perez.frank.bl.entities.categoria.Categoria;
import perez.frank.bl.entities.estado.Estado;
import perez.frank.bl.entities.subasta.Subasta;
import perez.frank.bl.entities.usuario.Usuario;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

public class Item {
    private int idItem;
    private String nombre;
    private Estado estado;
    private String descripcion;
    private String imagen;
    private LocalDate fechaDeCompra;
    private int antiguedad;
    private Categoria categoria;
    private Usuario propietario;
    private Subasta idSubasta;


    public Item(String nombre, Estado estado, String descripcion, String imagenes, LocalDate fechaDeCompra, int antiguedad, Categoria categoria, Usuario propietario, Subasta idSubasta) {
        this.nombre = nombre;
        this.estado = estado;
        this.descripcion = descripcion;
        this.imagen = imagenes;
        this.fechaDeCompra = fechaDeCompra;
        this.antiguedad = antiguedad;
        this.categoria = categoria;
        this.idSubasta = idSubasta;
    }


    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public LocalDate getFechaDeCompra() {
        return fechaDeCompra;
    }

    public void setFechaDeCompra(LocalDate fechaDeCompra) {
        this.fechaDeCompra = fechaDeCompra;
    }

    public int getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(int antiguedad) {
        this.antiguedad = antiguedad;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Usuario getPropietario() {
        return propietario;
    }

    public void setPropietario(Usuario propietario) {
        this.propietario = propietario;
    }

    public Subasta getIdSubasta() {
        return idSubasta;
    }

    public void setIdSubasta(Subasta idSubasta) {
        this.idSubasta = idSubasta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(nombre, item.nombre) && estado == item.estado && Objects.equals(descripcion, item.descripcion) && Objects.equals(imagen, item.imagen) && Objects.equals(fechaDeCompra, item.fechaDeCompra) && Objects.equals(antiguedad, item.antiguedad) && Objects.equals(categoria, item.categoria) && Objects.equals(propietario, item.propietario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombre, estado, descripcion, imagen, fechaDeCompra, antiguedad, categoria, propietario);
    }

    @Override
    public String toString() {
        return "Item{" + idItem + '\''+
                "nombre='" + nombre + '\'' +
                ", estado=" + estado +
                ", descripcion='" + descripcion + '\'' +
                ", imagenes='" + imagen + '\'' +
                ", fechaDeCompra=" + fechaDeCompra +
                ", antiguedad='" + antiguedad + '\'' +
                ", categoria=" + categoria +
                ", propietario=" + propietario +
                '}';
    }
}