package perez.frank.bl.entities.item;


import perez.frank.bl.entities.categoria.Categoria;
import perez.frank.bl.entities.estado.Estado;
import perez.frank.bl.entities.subasta.Subasta;
import perez.frank.bl.entities.usuario.Usuario;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class MySQLItemDAO {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public ArrayList<Item> listarItems()throws SQLException {
        ArrayList<Item> listaItems = new ArrayList<Item>();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = null;
            String query = "SELECT * FROM item";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost:3306/universidad?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);// el rs almacena la información de la base de datos.
            // TENGO QUE RECORRERLA
            while (rs.next()) { //rs.next devuelve true si hay más líneas en el result set. por defecto, al iniciar el ciclo, el rs está en la línea 0.
                Item item;

                Categoria ca = new Categoria();
                ca.setCodigo(rs.getInt("categoria"));
                Usuario us = new Usuario();
                us.setCedula(rs.getString("id_propietario"));
                Subasta su = new Subasta();
                su.setCodigo(rs.getInt("id_subasta"));
                item = new Item(
                        rs.getString("nombre"),
                        Estado.valueOf(rs.getString("esdado")),
                        rs.getString("descripcion"),
                        rs.getString("imagenes"),
                        LocalDate.parse(rs.getString("fecha_de_Compra")),
                        rs.getInt("antiguedad"),
                        ca,
                        us,
                        su);
                item.setIdItem(rs.getInt("id_item"));
                listaItems.add(item);
            }

            //conn.close();
        }
        catch (ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch (SQLException e){

            System.out.println(e.getMessage());
        }
        return listaItems;

    }

    public static void registrarItem(Item tmpItem)throws SQLException, IOException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            String query = "INSERT INTO item(nombre, estado, descripcion, imagen, fecha_de_compra, antiguedad, categoria, propietario, id_subasta) VALUES(?,?,?,?,?,?,?,?,?)";
            Connection conn = null;

            PreparedStatement stmt = null;
            String strConexion = "jdbc:mysql://localhost:3306/subastas?user=root&password=root&useSSL=false&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.prepareStatement(query);

            stmt.setString(1, tmpItem.getNombre());
            stmt.setString(2, tmpItem.getEstado().getNombre());
            stmt.setString(3, tmpItem.getDescripcion());
            stmt.setString(4, tmpItem.getImagen());
            stmt.setString(5, tmpItem.getFechaDeCompra().toString());
            stmt.setInt(6, tmpItem.getAntiguedad());
            stmt.setInt(7, tmpItem.getCategoria().getCodigo());
            stmt.setString(8, tmpItem.getPropietario().getCedula());
            stmt.setInt(9, tmpItem.getIdSubasta().getCodigo());

            stmt.execute();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
