package perez.frank.bl.entities.estado;

public enum Estado {
    NUEVO("Nuevo", 1), USADO("Usado", 2), ACTIVO("Activo", 3), INACTIVO("Inactivo", 4), OFERTANDO("Ofertando", 5), DENEGADA("Denegada", 6), APROBADA("Aprobada", 7);

    private String nombre;
    private int codigo;

    Estado(){

    }

    Estado(String nombre, int codigo) {
        this.nombre = nombre;
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public int getCodigo() {
        return codigo;
    }



}