package perez.frank.bl.logic;

import perez.frank.bl.entities.administrador.Administrador;
import perez.frank.bl.entities.administrador.MySQLAdministradorDAO;
import perez.frank.bl.entities.categoria.Categoria;
import perez.frank.bl.entities.categoria.MySQLCategoriaDAO;
import perez.frank.bl.entities.coleccionista.Coleccionista;
import perez.frank.bl.entities.coleccionista.MySQLColeccionistaDAO;
import perez.frank.bl.entities.estado.Estado;
import perez.frank.bl.entities.estado.MySQLEstadoDAO;
import perez.frank.bl.entities.item.Item;
import perez.frank.bl.entities.item.MySQLItemDAO;
import perez.frank.bl.entities.ofertaDeCompra.MySQLOfertaDeCompraDAO;
import perez.frank.bl.entities.ofertaDeCompra.OfertaDeCompra;
import perez.frank.bl.entities.ordenDeCompra.MySQLOrdenDeCompra;
import perez.frank.bl.entities.ordenDeCompra.OrdenDeCompra;
import perez.frank.bl.entities.subasta.MySQLSubastaDAO;
import perez.frank.bl.entities.subasta.Subasta;
import perez.frank.bl.entities.usuario.MySQLUsuarioDAO;
import perez.frank.bl.entities.usuario.Usuario;
import perez.frank.bl.entities.vendedor.MySQLVendedorDAO;
import perez.frank.bl.entities.vendedor.Vendedor;
import perez.frank.dl.Data;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

public class Gestor {

    private Data db;
    private MySQLUsuarioDAO usuarioDAO;
    private MySQLVendedorDAO vendedorDAO;
    private MySQLColeccionistaDAO coleccionistaDAO;
    private MySQLAdministradorDAO administradorDAO;
    private MySQLEstadoDAO estadoDAO;
    private MySQLCategoriaDAO categoriaDAO;
    private MySQLItemDAO itemDAO;
    private MySQLSubastaDAO subastaDAO;
    private MySQLOfertaDeCompraDAO ofertaDAO;
    private MySQLOrdenDeCompra ordenDAO;

    public Gestor() {
        this.db = new Data();
        this.usuarioDAO = new MySQLUsuarioDAO();
        this.vendedorDAO = new MySQLVendedorDAO();
        this.administradorDAO = new MySQLAdministradorDAO();
        this.coleccionistaDAO = new MySQLColeccionistaDAO();
        this.estadoDAO = new MySQLEstadoDAO();
        this.categoriaDAO = new MySQLCategoriaDAO();
        this.itemDAO = new MySQLItemDAO();
        this.subastaDAO = new MySQLSubastaDAO();
        this.ofertaDAO = new MySQLOfertaDeCompraDAO();
        this.ordenDAO = new MySQLOrdenDeCompra();
    }

//USUARIO

    public void registrarUsuario(String cedula, String nombre, String apellido1, String apellido2, String tipoUsuario, String clave) throws SQLException, Exception {
        Usuario tmpUsuario = new Usuario(cedula, nombre, apellido1, apellido2, tipoUsuario, clave);
        try {
            usuarioDAO.registrarUsuario(tmpUsuario);
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<String> listarUsuario() throws SQLException, Exception {
        try {
            ArrayList<Usuario> listaUsuarios = usuarioDAO.listarUsuarios();
            ArrayList<String> listaDatos = new ArrayList<String>();

            for (Usuario usuario : listaUsuarios) {
                listaDatos.add(usuario.toString());//enviamos datos, no objetos, por eso enviamos toString
            }
            return listaDatos;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    public Usuario buscarUsuario(String cedula, String password) throws Exception {
        try {
            ArrayList<Usuario> usuarios = usuarioDAO.listarUsuarios();
            for (Usuario usuario : usuarios) {
                if (usuario.getCedula().equals(cedula) && usuario.getClave().equals(password)) {
                    logearUsuario(usuario);
                    return usuario;
                }
            }
            return null;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }


    }

    public void logearUsuario(Usuario usuario){
        db.logerarUsuario(usuario);
    }

    public Usuario obtenerUsuarioLogeado(){
        return db.obtenerInforUsuarioLog();
    }

//-----------------------------------------------------------------------------------------------------

//VENDEDOR

    public void registrarVendedor(String cedula, String nombre, String apellido1, String apellido2, String clave, String direccion, String correoElectronico) throws SQLException, Exception {
        Vendedor tmpVendedor = new Vendedor(cedula, nombre, apellido1, apellido2, direccion, correoElectronico, clave);
        Usuario tmpUsuario = new Usuario(cedula, nombre, apellido1, apellido2, "Vendedor", clave);
        try {
            usuarioDAO.registrarUsuario(tmpUsuario);
            vendedorDAO.registrarVendedor(tmpVendedor);
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<String> listarVendedor() throws SQLException, Exception {

        try {
            ArrayList<Vendedor> listaVendedores = vendedorDAO.listarVendedores();
            ArrayList<String> listaDatos = new ArrayList<String>();
            for (Vendedor vendedor : listaVendedores) {
                listaDatos.add(vendedor.toString());//enviamos datos, no objetos, por eso enviamos toString
            }
            return listaDatos;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    public Vendedor buscarVendedor(String correo, String password) throws Exception {
        try {
            ArrayList<Vendedor> vendedores = vendedorDAO.listarVendedores();
            for (Vendedor vendedor : vendedores) {
                if (vendedor.getCorreoElectonico().equals(correo) && vendedor.getClave().equals(password)) {
                    return vendedor;
                }
            }
            return null;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }


    }

//-----------------------------------------------------------------------------------------------------------

//COLECCIONISTA

    public void registrarColeccionista(String cedula, String nombre, String apellido1, String apellido2, String clave, String correoElectronico, String avatar, LocalDate fechaDeNacimiento, int edad, double puntuacion, String intereses) throws SQLException, Exception {
        Coleccionista tmpColeccionista = new Coleccionista(cedula, nombre, apellido1, apellido2, clave, correoElectronico, avatar, fechaDeNacimiento, edad, puntuacion, intereses);
        Usuario tmpUsuario = new Usuario(cedula, nombre, apellido1, apellido2, "Coleccionista", clave);
        try {
            usuarioDAO.registrarUsuario(tmpUsuario);
            coleccionistaDAO.registrarColeccionista(tmpColeccionista);
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<String> listarColeccionista() throws SQLException, Exception {
        try {
            ArrayList<Coleccionista> listaColeccionistas = coleccionistaDAO.listarColeccionistas();
            ArrayList<String> listaDatos = new ArrayList<String>();

            for (Coleccionista coleccionista : listaColeccionistas) {
                listaDatos.add(coleccionista.toString());//enviamos datos, no objetos, por eso enviamos toString
            }
            return listaDatos;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    public Coleccionista buscarColeccionista(String correo, String password) throws Exception {

        for (Coleccionista col : coleccionistaDAO.listarColeccionistas()) {
            if (col.getCorreoElectronico().equals(correo) && col.getClave().equals(password)) {
                return col;
            }
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------------------


//CATEGORIA

    public void registrarCategoria(String nombre, String estado) throws SQLException, Exception {


        Categoria tmpCategoria = new Categoria(nombre, Estado.valueOf(estado));

        try {

            categoriaDAO.registrarCategoria(tmpCategoria);
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<String> listarCategoria() throws SQLException, Exception {
        try {
            ArrayList<Categoria> listaCategorias = categoriaDAO.listarCategorias();
            ArrayList<String> listaDatos = new ArrayList<String>();

            for (Categoria categoria : listaCategorias) {
                listaDatos.add(categoria.toString());//enviamos datos, no objetos, por eso enviamos toString
            }
            return listaDatos;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

//-------------------------------------------------------------------------------------------------

//ITEM

    public void registrarItem(String nombre, String estado, String descripcion, String imagenes, LocalDate fechaDeCompra, int antiguedad, int categoria, String propietario, int idSusbasta) throws SQLException, Exception {
        Categoria ca = new Categoria();
        ca.setCodigo(categoria);
        Usuario us = new Usuario();
        us.setCedula(propietario);
        Subasta su =new Subasta();
        su.setCodigo(idSusbasta);
        Item tmpItem = new Item(nombre, Estado.valueOf(estado), descripcion, imagenes, fechaDeCompra, antiguedad, ca, us, su);

        try {

            itemDAO.registrarItem(tmpItem);
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<String> listarItems() throws SQLException, Exception {
        try {
            ArrayList<Item> listaItems = itemDAO.listarItems();
            ArrayList<String> listaDatos = new ArrayList<String>();

            for (Item item : listaItems) {
                listaDatos.add(item.toString());//enviamos datos, no objetos, por eso enviamos toString
            }
            return listaDatos;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

//----------------------------------------------------------------------------------------------------------

//SUBASTA


    public void registrarSubasta(String creador, LocalDate fechaDeCreacion, LocalDate fechaDeInicio, LocalDate fechaDeVencimiento, double precioDeInicio) throws SQLException, Exception {
        Usuario us = new Usuario();
        us.setCedula(creador);
        Coleccionista col = new Coleccionista();
      // col.setCorreoElectronico(moderador);
        Subasta tmpSubasta = new Subasta(us, fechaDeCreacion, fechaDeInicio, fechaDeVencimiento, precioDeInicio, col);

        try {

            subastaDAO.registrarSubasta(tmpSubasta);
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<Subasta> listarSubastaDeUsuario(String cedula) throws SQLException, Exception {
        ArrayList<Subasta> listaSubastas = subastaDAO.listarSubastasDeUsuario(cedula);
        return listaSubastas;
    }

    public ArrayList<Subasta> listarSubastas() throws SQLException, Exception {

        ArrayList<Subasta> listaSubastas = subastaDAO.listarSubastas();
        return listaSubastas;

    }
//---------------------------------------------------------------------------------------------------------

//OFERTA DE COMPRA


    public void registrarOfertaDeCompra(String ofertante, double ofertaDeCompra, int subasta) throws SQLException, Exception {
        Coleccionista of = new Coleccionista();
        of.setCorreoElectronico(ofertante);
        Subasta su = new Subasta();
        su.setCodigo(subasta);
        OfertaDeCompra tmpOfertaDeCompra = new OfertaDeCompra(of, ofertaDeCompra, su);

        try {

            ofertaDAO.registrarOfertaDeCompra(tmpOfertaDeCompra);
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<String> listarOfertaDeCompra() throws SQLException, Exception {
        try {
            ArrayList<OfertaDeCompra> listaOfertaDeCompras = ofertaDAO.listarOfertaDeCompras();
            ArrayList<String> listaDatos = new ArrayList<String>();

            for (OfertaDeCompra ofertaDeCompra : listaOfertaDeCompras) {
                listaDatos.add(ofertaDeCompra.toString());//enviamos datos, no objetos, por eso enviamos toString
            }
            return listaDatos;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }


//-------------------------------------------------------------------------------------------------------

//ORDEN DE COMPRA


    public void registrarOrdenDeCompra(Coleccionista coleccionista, Vendedor vendedor) throws SQLException, Exception {
        OrdenDeCompra tmpOrdenDeCompra = new OrdenDeCompra(coleccionista, vendedor);

        try {

            ordenDAO.registrarOrdenDeCompra(tmpOrdenDeCompra);
        } catch (SQLException e) {
            throw e;
        }
    }

    public ArrayList<String> listarOrdenDeCompra() throws SQLException, Exception {
        try {
            ArrayList<OrdenDeCompra> listaOrdenDeCompras = ordenDAO.listarOrdenDeCompras();
            ArrayList<String> listaDatos = new ArrayList<String>();

            for (OrdenDeCompra ordenDeCompra : listaOrdenDeCompras) {
                listaDatos.add(ordenDeCompra.toString());//enviamos datos, no objetos, por eso enviamos toString
            }
            return listaDatos;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }


//-------------------------------------------------------------------------------------------------------

//METODOS ESPECIALES


    public static LocalDate convertirALocalDate(Date fecha) {
        return fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static int calcularEdad(LocalDate fechaNacimiento) {
        LocalDate fechaActual = LocalDate.now();
        return Period.between(fechaNacimiento, fechaActual).getYears();
    }



    /* Metodo Login, va a recibir un id o correo y la contraseña, va a llamar los tres metodos listar de
    administrador, vendedor y coleccionista, si el id esta en en vendedor, se logea como vendedor,
    si esta en coleccionista, como coleccionista. etc.
     */

}




/*private MySQLDAO dao;

    public void T create(String nombre, String apellido1, String apellido2) {
        Coleccionista coleccionista = new Coleccionista(nombre, apellido1, apellido2);

        try {
            dao.create(coleccionista);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }*/