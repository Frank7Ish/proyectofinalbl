package perez.frank.dl;

import perez.frank.bl.entities.administrador.Administrador;
import perez.frank.bl.entities.categoria.Categoria;
import perez.frank.bl.entities.estado.Estado;
import perez.frank.bl.entities.item.Item;
import perez.frank.bl.entities.ofertaDeCompra.OfertaDeCompra;
import perez.frank.bl.entities.ordenDeCompra.OrdenDeCompra;
import perez.frank.bl.entities.subasta.Subasta;
import perez.frank.bl.entities.usuario.Usuario;
import perez.frank.bl.entities.vendedor.Vendedor;

import java.util.ArrayList;

public class Data {
    private ArrayList<Estado> estados;
    private ArrayList<Categoria> categorias;
    private ArrayList<Item> items;
    private ArrayList<Usuario> usuarios;
    private ArrayList<Administrador> administradores;
    private ArrayList<Vendedor> vendedores;
    private ArrayList<perez.frank.bl.entities.coleccionista.Coleccionista> coleccionistas;
    private ArrayList<Subasta> subastas;
    private ArrayList<OfertaDeCompra> ofertas;
    private ArrayList<OrdenDeCompra> ordenes;

    public static Usuario myUsuarioLogeado = new Usuario();

    public Data(){

    }

    public void logerarUsuario(Usuario usuario){
        myUsuarioLogeado = usuario;
    }

    public Usuario obtenerInforUsuarioLog(){
        return myUsuarioLogeado;
    }

    public void agregarEstado(Estado estado){
        if(estado==null){
            estados = new ArrayList<Estado>();
        }
        estados.add(estado);
    }

    public ArrayList<Estado> getEstado(){
        return (ArrayList<Estado>) estados.clone();
    }
//---------------------------------------------------------------
    public void agregarCategoria(Categoria categoria){
        if(categoria==null){
            categorias = new ArrayList<Categoria>();
        }
        categorias.add(categoria);
    }

    public ArrayList<Categoria> getCategoria(){
        return (ArrayList<Categoria>) categorias.clone();
    }
//---------------------------------------------------------------

    public void agregarItem(Item item){
        if(item==null){
            items = new ArrayList<Item>();
        }
        items.add(item);
    }

    public ArrayList<Item> getItem(){
        return (ArrayList<Item>) items.clone();
    }
//---------------------------------------------------------

    public void agregarUsuario(Usuario usuario){
        if(usuario==null){
            usuarios = new ArrayList<Usuario>();
        }
        usuarios.add(usuario);
    }

    public ArrayList<Usuario> getUsuario(){
        return (ArrayList<Usuario>) usuarios.clone();
    }
//----------------------------------------------------------

    public void agregarAdministrador(Administrador administrador){
        if(administrador==null){
            administradores = new ArrayList<Administrador>();
        }
        administradores.add(administrador);
    }

    public ArrayList<Administrador> getAdministrador(){
        return (ArrayList<Administrador>) administradores.clone();
    }
//-----------------------------------------------------------------

    public void agregarVendedor(Vendedor vendedor){
        if(vendedor==null){
            vendedores = new ArrayList<Vendedor>();
        }
        vendedores.add(vendedor);
    }

    public ArrayList<Vendedor> getVendedor(){
        return (ArrayList<Vendedor>) vendedores.clone();
    }
//----------------------------------------------------------------------------

    public void agregarColeccionista(perez.frank.bl.entities.coleccionista.Coleccionista coleccionista){
        if(coleccionista==null){
            coleccionistas = new ArrayList<perez.frank.bl.entities.coleccionista.Coleccionista>();
        }
        coleccionistas.add(coleccionista);
    }

    public ArrayList<perez.frank.bl.entities.coleccionista.Coleccionista> getColeccionista(){
        return (ArrayList<perez.frank.bl.entities.coleccionista.Coleccionista>) coleccionistas.clone();
    }
//-------------------------------------------------------------------------------------

    public void agregarSubasta(Subasta subasta){
        if(subasta==null){
            subastas = new ArrayList<Subasta>();
        }
        subastas.add(subasta);
    }

    public ArrayList<Subasta> getSubastas(){
        return (ArrayList<Subasta>) subastas.clone();
    }

//-----------------------------------------------------------------------------
public void agregarOfertaDeCompra(OfertaDeCompra ofertaDeCompra){
    if(ofertaDeCompra==null){
        ofertas = new ArrayList<OfertaDeCompra>();
    }
    ofertas.add(ofertaDeCompra);
}

    public ArrayList<OfertaDeCompra> getOfertaDeCompra(){
        return (ArrayList<OfertaDeCompra>) ofertas.clone();
    }
//--------------------------------------------------------------------------


    public void agregarOrdenDeCompra(OrdenDeCompra ordenDeCompra){
        if(ordenDeCompra==null){
            ordenes = new ArrayList<OrdenDeCompra>();
        }
        ordenes.add(ordenDeCompra);
    }

    public ArrayList<Subasta> getSubasta(){
        return (ArrayList<Subasta>) subastas.clone();
    }
}


